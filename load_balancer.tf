resource "hcloud_load_balancer" "master" {
  name                = "load-balancer-master"
  load_balancer_type  = "lb11"
  location            = var.location
  count               = var.enable_multi_master? 1: 0
}

resource "hcloud_load_balancer_network" "master" {
  load_balancer_id  = hcloud_load_balancer.master[0].id
  subnet_id         = hcloud_network_subnet.kube_subnet.id
  ip                = local.load_balancer_private_ip
  count             = var.enable_multi_master? 1: 0
}

resource "hcloud_load_balancer_service" "master" {
  load_balancer_id  = hcloud_load_balancer.master[0].id
  protocol          = "tcp"
  listen_port       = 6443
  destination_port  = 6443
  count             = var.enable_multi_master? 1: 0
}

resource "hcloud_load_balancer_target" "master" {
  depends_on = [
    hcloud_load_balancer_network.master,
    hcloud_server.master,
  ]

  type              = "server"
  load_balancer_id  = hcloud_load_balancer.master[0].id
  server_id         = hcloud_server.master[count.index].id
  use_private_ip    = true
  count             = var.enable_multi_master? 3: 0
}
