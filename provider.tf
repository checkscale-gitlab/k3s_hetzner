terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
    }
  }
}

provider "hcloud" {
}

provider "kubectl" {
}
